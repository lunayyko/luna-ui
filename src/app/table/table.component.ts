import { Component, OnInit } from '@angular/core';

import { data } from './table.model'

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.sass']
})
export class TableComponent implements OnInit {


  cols: {field: String, header: String}[];
  releases

  constructor() { }

  ngOnInit() {

    this.releases = data

    this.cols = [
      { field: 'releasedate', header: 'Release Date' },
      { field: 'territories', header: 'Territory' },
      { field: 'label', header: 'Label' },
      { field: 'catalogno', header: 'Catalog No' },
      { field: 'genre', header: 'Genre' },
      { field: 'releaseartist', header: 'Artist' },      
      { field: 'releasetitle', header: 'Title' },
      { field: 'appleIdRelease', header: 'Apple ID Release' },
      { field: 'ig', header: 'Instant Gratification' },
      { field: 'igAppleId', header: 'IG Apple ID Track' },
      { field: 'link', header: 'Streaming / Download Link' }]
  }

}
